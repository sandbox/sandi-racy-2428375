Copyright 2015 ndi_racy@rocketmail.com

Description
----------
This module provides field for indonesian phone number

Valid format number
----------
(021) 123456
(021) 123 456
(021) 1234567
(021) 123 456 7
(021) 12345678
(021) 123 456 78
(021) 123 456
(+6221) 123456
(+6221) 1234567
(+6221) 123 456 7
(+6221) 12345678
(+6221) 123 456 78
0812345678
081 234 567 8
08123456789
081 234 567 89
081234567891
081 234 567 891
+62812345678
+6281 234 567 8
+628123456789
+6281 234 567 89
+6281234567891
+6281 234 567 891

http://en.wikipedia.org/wiki/Telephone_numbers_in_Indonesia

Installation
----------
To install, copy the indonesian_phone directory to your modules directory

Configuration
-----------
To enable this module, visit administer -> modules, and enable Indonesian Phone

Bugs/Features/Patches:
----------
If you want to report bugs, feature requests, or submit a patch,
please do at the project page on the Drupal web site
http://drupal.org/project/indonesian_phone

Author
----------
Sandi Rosyandi (ndi_racy@rocketmail.com)

The author can also be contacted for paid customizations of this and other
modules
